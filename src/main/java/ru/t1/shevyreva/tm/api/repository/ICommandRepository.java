package ru.t1.shevyreva.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    @Nullable
    AbstractCommand getCommandByName(@NotNull String name);

    @Nullable
    AbstractCommand getCommandByArgument(@NotNull String argument);

    void add(@NotNull AbstractCommand command);

    @NotNull
    Collection<AbstractCommand> getTerminalCommands();

}
