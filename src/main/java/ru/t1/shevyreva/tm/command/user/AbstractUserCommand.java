package ru.t1.shevyreva.tm.command.user;

import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.service.IAuthService;
import ru.t1.shevyreva.tm.api.service.IUserService;
import ru.t1.shevyreva.tm.command.AbstractCommand;
import ru.t1.shevyreva.tm.exception.entity.UserNotFoundException;
import ru.t1.shevyreva.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    @Nullable
    protected IAuthService getAuthService() {
        if (getServiceLocator() == null) return null;
        return getServiceLocator().getAuthService();
    }

    @Nullable
    protected IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void showUser(@Nullable final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("Login: " + user.getLogin());
        System.out.println("Id: " + user.getId());
    }

}
