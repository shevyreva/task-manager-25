package ru.t1.shevyreva.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.repository.IRepository;
import ru.t1.shevyreva.tm.exception.entity.ModelNotFoundException;
import ru.t1.shevyreva.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final List<M> records = new ArrayList<>();

    @NotNull
    public M add(@NotNull final M model) {
        records.add(model);
        return model;
    }

    @NotNull
    public List<M> findAll() {
        return records;
    }

    @NotNull
    public List<M> findAll(@NotNull final Comparator<M> comparator) {
        @NotNull final List<M> result = new ArrayList<>(records);
        result.sort(comparator);
        return result;
    }

    @Nullable
    public M findOneById(@NotNull final String id) {
        return records.stream()
                .filter(m -> id.equals(m.getId()))
                .findFirst().orElse(null);
    }

    @NotNull
    public M findOneByIndex(@NotNull final Integer index) {
        return records.get(index);
    }

    public void removeAll() {
        records.clear();
    }

    @NotNull
    public M removeOne(@NotNull final M model) {
        records.remove(model);
        return model;
    }

    @NotNull
    public M removeOneById(@NotNull final String id) {
        @Nullable final M model = findOneById(id);
        if (model == null) throw new ModelNotFoundException();
        removeOne(model);
        return model;
    }

    public void removeOneByIndex(@NotNull final Integer index) {
        @NotNull final M model = findOneByIndex(index);
        removeOne(model);
    }

    public boolean existsById(final String id) {
        @Nullable final M model = findOneById(id);
        return model != null;
    }

    @NotNull
    public Integer getSize() {
        return records.size();
    }

}
