package ru.t1.shevyreva.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.util.TerminalUtil;

public class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private final String NAME = "project-update-by-index";

    @NotNull
    private final String DESCRIPTION = "Update project by Index.";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE BY INDEX]");
        System.out.println("Enter Index:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("Enter Name:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter Description:");
        @NotNull final String description = TerminalUtil.nextLine();
        @Nullable final String userId = getUserId();
        getProjectService().updateByIndex(userId, index, name, description);
    }

}
