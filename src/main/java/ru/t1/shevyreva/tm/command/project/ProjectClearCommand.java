package ru.t1.shevyreva.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    private final String DESCRIPTION = "Clear project.";

    @NotNull
    private final String NAME = "project-clear";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        @Nullable final String userId = getUserId();
        getProjectService().removeAll(userId);
    }
}
